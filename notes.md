# **Ngày 13/2**

Huớng dẫn các lệnh cơ bản của Git và Terminal

## **Command line - Terminal**

- PWD: Print Working Directory - In địa chỉ hiện tại của thư mục
- CD: Change directory - Di chuyển đến thư mục
- mkdir: Create folder - Tạo Folder


## **Git**
### Clone dự án về máy
```sh
$ git clone https://gitlab.com/dopagovn/beginer-code-learn.git
```
### **Trouble Shooting**

Giải quyết vấn đề của Git khi yêu cầu config Name và Email

```sh
$ git config --global user.name "Thinh"
$ git config --global user.email "dopagovn@gmail.com"
```

### **Command for Git**

- Commit 
```sh
$ git commit -m "Nội dung message"
```
- Đẩy code lên trên repository
```sh
$ git push
```
- Đẩy các file lên Storage
```sh
$ git add <filenames>
```
> hoặc hết tất cả các file
```sh
$ git add .
```
## **Git Resolve Conflict**

```sh
$ git pull --rebase origin main
$ git rebase --continue
$ git push --force
```


## Công cụ Paste Image Extensions

![](2022-02-13-16-30-00.png)